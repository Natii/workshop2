<?php
$temperaturas = array("78", "60", "62", "68", "71", "68", "73", "85", "66", "64", "76", "63", "75", "76", "73", "68", "62", "73", "72", "65", 
"74", "62", "62", "65", "64", "68", "73", "75", "79", "73");

// Obtener longitud
$cantidadDeElementos = count($temperaturas);
$suma = 0;
foreach ($temperaturas as $numero) {
    $suma += $numero;
}
// Dividir, y listo
$promedio = $suma / $cantidadDeElementos;
echo "Promedio de temperatura: $promedio".'<br/>';

$numerosUnicos = array_unique($temperaturas);

sort($numerosUnicos);
$temperaturasBajas = $numerosUnicos;
$resultadoBajas = array_slice($temperaturasBajas, 0, 7);
echo "<pre>";
echo "Las 7 temperaturas más bajas son: <br>";
print_r($resultadoBajas);

rsort($numerosUnicos);
$temperaturasAltas = $numerosUnicos;
$resultadoAltas = array_slice($temperaturasAltas, 0, 7);
echo "<pre>";
echo "Las 7 temperaturas más altas son: <br>";
print_r($resultadoAltas);
?>